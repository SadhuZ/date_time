#include <iostream>
#include <ctime>
#include <locale>
#include <iomanip>
#include <vector>

//#include <stdlib.h>
//#include <time.h>

using namespace std;



struct PersonBirthday{
    string name;
    tm birthday;
};

void PersonBirthdaySort(vector<PersonBirthday>& persons){
    for(size_t i = 0; i < persons.size() - 1; ++i){
        for(size_t j = 0; j < persons.size() - i - 1; ++j){
            if(persons[j].birthday.tm_mon > persons[j + 1].birthday.tm_mon){
                PersonBirthday p = persons[j];
                persons[j] = persons[j + 1];
                persons[j + 1] = p;
            }
        }
    }

    for(size_t i = 0; i < persons.size() - 1; ++i){
        for(size_t j = 0; j < persons.size() - i - 1; ++j){
            if((persons[j].birthday.tm_mday > persons[j + 1].birthday.tm_mday) &&
               (persons[j].birthday.tm_mon == persons[j + 1].birthday.tm_mon)){
                PersonBirthday p = persons[j];
                persons[j] = persons[j + 1];
                persons[j + 1] = p;
            }
        }
    }
}


int main()
{
    time_t curTime = time(nullptr);
    tm today = *localtime(&curTime);
    tm birthday = *localtime(&curTime);
    vector<PersonBirthday> friends;

//    srand(time(NULL));
//    for(size_t i = 0; i < 30; ++i){
//        birthday.tm_mday = 1 + 30. * rand()/RAND_MAX;
//        birthday.tm_mon = 1 + 11. * rand()/RAND_MAX;
//        PersonBirthday myFriend;
//        myFriend.name = "myFriend" + to_string(i);
//        myFriend.birthday = birthday;
//        friends.push_back(myFriend);
//    }
//    for(size_t i = 0; i < 30; ++i){
//        PersonBirthday myFriend;
//        myFriend.name = "myFriend" + to_string(30 + i);
//        myFriend.birthday = friends[i].birthday;
//        friends.push_back(myFriend);
//    }

    while(1){
        string friendName;
        cout << "Enter friend name: ";
        cin >> friendName;
        if(friendName == "end"){
            break;
        }

        cout << "Enter friend birthday (yyyy/mm/dd): ";
        cin >> get_time(&birthday, "%Y/%m/%d");
        if (cin.fail()) {
            cout << "Birthday parse failed\n";
        } else {
            PersonBirthday myFriend;
            myFriend.name = friendName;
            myFriend.birthday = birthday;
            friends.push_back(myFriend);
        }
    }

    PersonBirthdaySort(friends);

//    for(size_t i = 0; i < friends.size(); ++i){
//        cout << friends[i].name << " " << asctime(&friends[i].birthday);
//    }
//    cout << "Today " << asctime(&today) << endl;

    size_t i = 0;
    while(i < friends.size()){
        if((today.tm_mon == friends[i].birthday.tm_mon) && (today.tm_mday == friends[i].birthday.tm_mday)){
            cout << friends[i].name << " " << put_time(&friends[i].birthday, "%m/%d") << " Нарру birthday!" << endl;
            break;
        }
        else if((today.tm_mon == friends[i].birthday.tm_mon) && (today.tm_mday < friends[i].birthday.tm_mday)){
            cout << friends[i].name << " " << put_time(&friends[i].birthday, "%m/%d");
             break;
        }
        else if(today.tm_mon < friends[i].birthday.tm_mon){
            cout << friends[i].name << " " << put_time(&friends[i].birthday, "%m/%d");
             break;
        }
        i++;
    }
    while (i < friends.size() - 1) {
        if((friends[i].birthday.tm_mon == friends[i + 1].birthday.tm_mon) &&
           (friends[i].birthday.tm_mday == friends[i + 1].birthday.tm_mday))
            cout << friends[i + 1].name << " " << put_time(&friends[i + 1].birthday, "%m/%d");
        else
            break;
        i++;
    }

    return 0;
}
