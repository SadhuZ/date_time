#include <iostream>
#include <ctime>
#include <vector>

using namespace std;

void AddTaskData(vector<string> &taskData, string taskName, double time){
    if(taskName != ""){
        taskData.push_back(taskName + " - " + to_string(time));
    }
}

int main()
{
    vector<string> taskData;
    string taskName;
    string command;
    time_t startTime = time(nullptr);
    time_t endTime;

    while(1){
        cout << "Enter a command: ";
        cin >> command;
        if(command == "begin"){
            endTime = time(nullptr);
            AddTaskData(taskData, taskName, difftime(endTime, startTime));
            cout << "Enter task name: ";
            cin >> taskName;
            startTime = time(nullptr);
        }
        else if(command == "end"){
            endTime = time(nullptr);
            AddTaskData(taskData, taskName, difftime(endTime, startTime));
            taskName = "";
        }
        else if(command == "status"){
            for(auto task : taskData){
                cout << task << endl;
            }
            if(taskName != ""){
                cout << taskName << " - still runing" << endl;
            }
        }
        else if(command == "exit"){
            break;
        }
    }

    return 0;
}
