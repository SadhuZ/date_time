#include <iostream>
#include <iomanip>
#include<time.h>

using namespace std;

int main()
{
    tm timer;
    time_t targetTime;
    time_t currentTime;
    cout << "Set timer (mm.ss): ";
    cin >> get_time(&timer, "%M.%S");
    if (cin.fail()) {
        cout << "Timer parse failed\n";
        return 0;
    }
    else {
        currentTime = time(nullptr);
        targetTime = time(nullptr) + timer.tm_min * 60 + timer.tm_sec;
    }

    time_t deltaTime = targetTime - currentTime;
    time_t oldDeltaTime = deltaTime;
    while(deltaTime){
        currentTime = time(nullptr);
        deltaTime = targetTime - currentTime;
        if(oldDeltaTime != deltaTime){
            cout << deltaTime / 60 << ":" << deltaTime % 60 << endl;
            oldDeltaTime = deltaTime;
        }
    }
    cout << "Zzzzzzzzz!!!" << endl;
    return 0;
}
